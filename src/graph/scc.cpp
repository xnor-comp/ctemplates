struct SCC {
    vector<vector<int>> g, G;
    vector<char> used;
    vector<int> comp, topsort;
    int sz;

    void dfs1(int v) {
        used[v] = 1;
        for (auto e : g[v])
            if (!used[e])
                dfs1(e);
        topsort.push_back(v);
    }
    void dfs2(int v, int c) {
        used[v] = 2;
        comp[v] = c;
        for (auto e : G[v]) {
            if (used[e] < 2) {
                dfs2(e, c);
            }
        }
    }
    int scc() {
        for (int i = 0; i < sz; ++i) {
            if (!used[i]) {
                dfs1(i);
            }
        }
        int c{};
        for (int i = sz - 1; i >= 0; --i) {
            int v = topsort[i];
            if (used[v] < 2) {
                dfs2(v, c++);
            }
        }
        return c;
    }

    void clear(int n) {
        sz = n;
        g.assign(n, vector<int>());
        G.assign(n, vector<int>());
        used.assign(n, 0);
        comp.assign(n, 0);
        topsort.clear();
    }

    SCC() = default;
    SCC(int n) {
        clear(n);
    }

    void add_edge(int v, int u) {
        g[v].push_back(u);
        G[u].push_back(v);
    }

    int operator()() {
        return scc();
    }

    int get(int v) const {
        return comp[v];
    }
};
