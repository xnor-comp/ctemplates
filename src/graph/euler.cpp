struct Eulerian {
    struct edge {
        int x;
        char r;
    };
    vector<edge> edges;
    vector<int> eul;
    vector<vector<int>> g;
    vector<char> deg;
 
    void clear(int n) {
        edges.clear();
        g.assign(n, vector<int>());
        deg.assign(n, 0);
    }
    Eulerian() = default;
    Eulerian(int n) {
        clear(n);
    }
 
    void euler(int v, vector<int>& ans) {
        while (g[v].size()) {
            int u = g[v].back();
            g[v].pop_back();
            if (edges[u].r)
                edges[u].r = 0, euler(edges[u].x ^ v, ans);
        }
        ans.push_back(v);
    }
 
    void add_edge(int v, int u) {
        g[v].push_back(edges.size()), g[u].push_back(edges.size());
        deg[v] ^= 1, deg[u] ^= 1;
        edges.emplace_back(v ^ u, 1);
    }
    bool operator()(vector<int>& ans, int root = 0) {
        if (accumulate(deg.begin(), deg.end(), int()))
            return 0;
        return euler(root, ans), 1;
    }
};
