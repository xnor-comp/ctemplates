struct Kuhn {
    vector<vector<int>> g;
    vector<int> mt, used;
    int timer = 1, sz = 0;

    void clear(int s, int t) {
        sz = s;
        timer = 1;
        g.assign(s, vector<int>());
        mt.assign(t, -1);
        used.assign(s, 0);
    }

    Kuhn() = default;
    Kuhn(int s, int t) {
        clear(s, t);
    }

    bool dfs(int v) {
        if (used[v] == timer)
            return 0;
        used[v] = timer;
        for (auto e : g[v])
            if (mt[e] == -1 || dfs(mt[e]))
                return mt[e] = v, 1;
        return 0;
    }

    void add_edge(int v, int u) {
        g[v].push_back(u);
    }

    int operator()() {
        int cnt{};
        for (int i = 1; cnt += dfs(i - 1), i < sz; ++i, ++timer) {}
        return cnt;
    }

    int match(int v) const {
        return mt[v];
    }
};
