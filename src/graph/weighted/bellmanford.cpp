template <typename T = long long, bool Dir = 0, T INF = 1144000000000000000>
struct BellmanFord {
    struct edge {
        int v, u;
        T w;
    };
    vector<edge> edges;
    vector<T> d;

    void clear(int n) {
        edges.clear();
    }
    BellmanFord() = default;

    void add_edge(int v, int u, T w) {
        edges.emplace_back(v, u, w);
        if constexpr (!Dir) edges.emplace_back(u, v, w);
    }

    vector<T> operator()(int n, int s = 0) {
        d.assign(n, INF);
        d[s] = 0;
        char ok = 1;
        while (exchange(ok, 0))
            for (int j = 0; j < (int)edges.size(); ++j) {
                if (d[edges[j].u] > d[edges[j].v] + edges[j].w) {
                    d[edges[j].u] = d[edges[j].v] + edges[j].w;
                    ok = 1;
                }
            }
        return d;
    }
};
