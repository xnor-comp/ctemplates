template <typename T, bool directed = 0>
struct Dijkstra {
    using pii = pair<T, int>;
    int n;
    vector<vector<pair<int, T>>> g;
    vector<T> d;
    vector<int> p;
 
    void clear(int sz) {
        n = sz;
        g.assign(n, vector<pair<int, T>>());
    }
    Dijkstra(int sz = 0) {
        clear(sz);
    }
 
    void add_edge(int v, int u, T w) {
        g[v].emplace_back(u, w);
        if constexpr (!directed)
            g[u].emplace_back(v, w);
    }
 
    vector<T> operator()(int s) {
        d.assign(n, numeric_limits<T>::max());
        p.assign(n, -1);
        d[s] = 0;
        priority_queue<pii, vector<pii>, greater<pii>> pq;
        pq.emplace(0, s);
        while (pq.size()) {
            int v;
            T w_v;
            tie(w_v, v) = pq.top();
            pq.pop();
            if (d[v] != w_v)
                continue;
            for (auto [u, w] : g[v]) {
                if (d[u] > d[v] + w) {
                    pq.emplace(d[u] = d[v] + w, u);
                    p[u] = v;
                }
            }
        }
        return d;
    }
 
    int get_parent(int v) const {
        return p[v];
    }
    T get_dist(int v) const {
        return d[v];
    }
};
