struct UnionFind {
    vector<int> e;
    void clear(int n) { e.assign(n, -1); }
    UnionFind(int n = 0) { clear(n); }
    int find(int x) { return e[x] < 0 ? x : e[x] = find(e[x]); }
    int size(int x) { return -e[find(x)]; }
    bool same(int x, int y) { return find(x) == find(y); }
    bool join(int a, int b) {
        a = find(a), b = find(b);
        if (a == b) return 0;
        if (e[a] > e[b]) swap(a, b);
        return e[a] += exchange(e[b], a);
    }
};
struct Tarjan {
    int n;
    vector<char> used;
    vector<int> par, x, ans;
    vector<vector<int>> g, queries;
    UnionFind dsu;
 
    void clear(int sz) {
        n = sz;
        x.clear();
        ans.clear();
        par.resize(n);
        g.assign(n, vector<int>());
        queries.assign(n, vector<int>());
    }
    Tarjan() = default;
    Tarjan(int sz) {
        clear(sz);
    }
 
    void dfs(int v) {
        used[v] = 1;
        par[v] = v;
        for (auto e : g[v])
            if (!used[e])
                dfs(e), dsu.join(e, v), par[dsu.find(v)] = v;
        for (auto e : queries[v])
            if (used[x[e] ^ v])
                ans[e] = par[dsu.find(x[e] ^ v)];
    }
 
    void add_edge(int v, int u) {
        g[v].push_back(u);
        g[u].push_back(v);
    }
    void add_query(int v, int u) {
        queries[v].push_back(x.size());
        queries[u].push_back(x.size());
        x.push_back(v ^ u);
        ans.emplace_back();
    }
 
    vector<int> operator()(int root) {
        dsu.clear(n);
        used.assign(n, 0);
        fill(ans.begin(), ans.end(), -1);
        dfs(root);
        return ans;
    }
    int query(int v) const {
        return ans[v];
    }
};
