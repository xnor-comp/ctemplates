template <int Lg = 25>
struct BinaryLifting {
    int timer;
    vector<int> tin, tout, d;
    vector<vector<int>> g;
    vector<array<int, Lg>> up;

    void clear(int n) {
        g.assign(n, vector<int>());
        up.resize(n);
        tin.assign(n, -1);
        tout.resize(n);
        d.resize(n);
    }

    BinaryLifting() = default;
    BinaryLifting(int n) {
        clear(n);
    }

    void dfs(int v, int p, int dep = 0) {
        tin[v] = timer++;
        up[v][0] = p;
        d[v] = dep;
        for (int i = 1; i < Lg; ++i)
            up[v][i] = up[up[v][i - 1]][i - 1];
        for (auto e : g[v])
            if (e != p)
                dfs(e, v, dep + 1);
        tout[v] = timer++;
    }

    void add_edge(int v, int u) {
        g[v].push_back(u);
        g[u].push_back(v);
    }
    void operator()(vector<int> roots) {
        timer = 0;
        for (auto e : roots)
            if (tin[e] == -1)
                dfs(e, e);
    }

    bool is_ancestor(int v, int u) const {
        return tin[v] <= tin[u] && tout[v] >= tout[u];
    }
    int lca(int v, int u) const {
        if (!is_ancestor(up[v][Lg - 1], u))
            return -1;
        if (is_ancestor(v, u))
            return v;
        if (is_ancestor(u, v))
            return u;
        for (int i = Lg - 1; i >= 0; --i)
            if (!is_ancestor(up[v][i], u))
                v = up[v][i];
        return up[v][0];
    }
    int distance(int v, int u) const {
        int l = lca(v, u);
        if (l == -1)
            return -1;
        return d[v] + d[u] - 2 * d[l];
    }
    int la(int v, int k) const {
        for (int i = 0; i < Lg; ++i, k >>= 1)
            if (k & 1)
                v = up[v][i];
        return v;
    }
};
