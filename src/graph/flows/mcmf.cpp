#include <ext/pb_ds/priority_queue.hpp>
template <typename T = long long, T INF = 0x3f3f3f3f>
struct MCMF {
    int n;
    vector<vector<int>> ed, red;
    vector<vector<T>> cap, flow, cost;
    vector<int> seen;
    vector<T> dist, pi;
    vector<pair<int, int>> par;

    void clear(int sz) {
        n = sz;
        ed.assign(n, vector<int>());
        red.assign(n, vector<int>());
        cap.assign(n, vector<T>(n));
        flow.assign(n, vector<T>(n));
        cost.assign(n, vector<T>(n));
        seen.assign(n, 0);
        dist.assign(n, 0);
        pi.assign(n, 0);
        par.assign(n, pair<int, int>());
    }
    MCMF() = default;
    MCMF(int sz) {
        clear(sz);
    }

    void add_edge(int a, int b, T c, T d) {
        cap[a][b] = c;
        cost[a][b] = d;
        ed[a].push_back(b);
        red[b].push_back(a);
    }

    void path(int s) {
        fill(seen.begin(), seen.end(), 0);
        fill(dist.begin(), dist.end(), INF);
        dist[s] = 0;
        T di;
        __gnu_pbds::priority_queue<pair<T, int>> q;
        vector<typename decltype(q)::point_iterator> its(n);
        q.push({0, s});
        auto relax = [&](int i, T w, T c, int dir) {
            T val = di - pi[i] + c;
            if (w && val < dist[i]) {
                dist[i] = val;
                par[i] = {s, dir};
                if (its[i] == q.end())
                    its[i] = q.push(pair<T, int>{-dist[i], i});
                else
                    q.modify(its[i], {-dist[i], i});
            }
        };
        while (q.size()) {
            s = q.top().second; q.pop();
            seen[s] = 1; di = dist[s] + pi[s];
            for (int i : ed[s]) if (!seen[i])
                relax(i, cap[s][i] - flow[s][i], cost[s][i], 1);
            for (int i : red[s]) if (!seen[i])
                relax(i, flow[i][s], -cost[i][s], 0);
        }
        for (int i = 0; i < n; ++i)
            pi[i] = min((decltype(int() + T()))(pi[i] + dist[i]),
                        (decltype(int() + T()))INF);
    }
    void setpi(int s) {
        fill(pi.begin(), pi.end(), INF);
        pi[s] = 0;
        int it = n, ch = 1;
        T v;
        while (ch-- && it--)
            for (int i = 0; i < n; ++i)
                if (pi[i] != INF)
                    for (int to : ed[i]) if (cap[i][to])
                        if ((v = pi[i] + cost[i][to]) < pi[to])
                            pi[to] = v, ch = 1;
    }

    pair<T, T> maxflow(int s, int t) {
        T totflow = 0, totcost = 0;
        while (path(s), seen[t]) {
            T fl = INF;
            for (int p, r, x = t; tie(p, r) = par[x], x != s; x = p)
                fl = min((decltype(int() + T()))fl,
                         (decltype(int() + T()))(r ? cap[p][x] - flow[p][x] :
                                                     flow[x][p]));
            totflow += fl;
            for (int p, r, x = t; tie(p, r) = par[x], x != s; x = p)
                if (r)
                    flow[p][x] += fl;
                else
                    flow[x][p] -= fl;
        }
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                totcost += cost[i][j] * flow[i][j];
        return {totflow, totcost};
    }
    pair<T, T> operator()(int s, int t) {
        setpi(s);
        return maxflow(s, t);
    }
};
