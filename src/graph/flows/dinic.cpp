template <bool B = 0, typename T = long long, T base = 1ll << 61ll>
struct Dinic {
    struct edge {
        int a, b;
        T f, c;
        edge(int ca, int cb, T cf, T cc): a(ca), b(cb), f(cf), c(cc) {}
    };

    int n, s, t;
    T flow = 0, lim;

    vector<edge> e;
    vector<int> pt, comp;
    vector<char> used;
    vector<T> d;
    vector<vector<int>> g, G;
    deque<int> q;

    void add_edge(int a, int b, T c) {
        g[a].push_back(e.size());
        e.emplace_back(a, b, 0, c);
        g[b].push_back(e.size());
        e.emplace_back(b, a, (!B) * c, c);
    }

    void clear(int sz) {
        n = sz;
        flow = 0;
        e.clear();
        pt.assign(sz, 0);
        d.assign(sz, 0);
        g.assign(sz, vector<int>());
        q.clear();
    }
    Dinic() = default;
    Dinic(int sz) {
        clear(sz);
    }

    bool bfs() {
        fill(d.begin(), d.end(), base);
        d[s] = 0;
        q.push_back(s);
        while (q.size() && d[t] == base) {
            int f = q.front();
            q.pop_front();
            for (auto id : g[f])
                if (d[e[id].b] == base && e[id].c - e[id].f >= lim)
                    d[e[id].b] = d[f] + 1, q.push_back(e[id].b);
        }
        return q.clear(), d[t] != base;
    }
    bool dfs(int v, T f) {
        if (!f)
            return 0;
        if (v == t)
            return 1;
        for (; pt[v] < (int)g[v].size(); pt[v]++) {
            int id = g[v][pt[v]];
            if (d[e[id].b] == d[v] + 1 && e[id].c - e[id].f >= f) {
                if (dfs(e[id].b, f))
                    return e[id].f += f, e[id ^ 1].f -= f, 1;
            }
        }
        return 0;
    }
    T operator()(int ss, int tt) {
        s = ss, t = tt;
        for (lim = base; lim >= 1;) {
            if (!bfs()) {
                lim >>= 1;
                continue;
            }
            fill(pt.begin(), pt.end(), 0);
            char pushed;
            while ((pushed = dfs(s, lim)))
                flow += lim;
        }
        return flow;
    }
    T get_flow(int id) const {
        return e[id << 1].f;
    }
    bool saturated(int id) const {
        return e[id << 1].f     == e[id << 1].c ||
         (B && e[id << 1 ^ 1].f == e[id << 1 ^ 1].c);
    }
    void cut(int v) {
        comp.push_back(v);
        used[v] = 1;
        for (auto u : G[v]) {
            if (!used[u]) {
                cut(u);
            }
        }
    }
    vector<int> mincut() {
        G.assign(n, vector<int>());
        used.assign(n, 0);
        comp.clear();
        for (int i = 0; 2 * i < (int)e.size(); ++i) {
            if (!saturated(i)) {
                G[e[i << 1].a].emplace_back(e[i << 1].b);
                if constexpr (B)
                    G[e[i << 1].b].emplace_back(e[i << 1].a);
            }
        }
        cut(s);
        return comp;
    }
    bool side(int k) const {
        return used[k];
    }
    bool st_cutter(int k) const {
        return saturated(k) && side(e[k << 1].a) != side(e[k << 1].b);
    }
};
