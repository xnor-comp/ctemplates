#ifdef xnor
#define dbg(x) ((cerr << __PRETTY_FUNCTION__ << ": " << __LINE__ << "; " << #x << " = " << x << '\n'), x)
#else
inline void dbg(...) {}
#endif
