namespace fastio {

constexpr int input_buffer_size = 1 << 23;
constexpr int output_buffer_size = 1 << 23;

inline char get_char_safe() __attribute__((always_inline));
inline char get_char_unsafe() __attribute__((always_inline));
inline char get_char() __attribute__((always_inline));
inline int read_int() __attribute__((always_inline));
inline long long read_ll() __attribute__((always_inline));
inline int read_uint() __attribute__((always_inline));
inline long long read_ull() __attribute__((always_inline));
inline void read_int(int&) __attribute__((always_inline));
inline void read_ll(long long&) __attribute__((always_inline));
inline void read_uint(int&) __attribute__((always_inline));
inline void read_ull(long long&) __attribute__((always_inline));
inline void flush_output() __attribute__((always_inline));
inline void write_char_safe(char c) __attribute__((always_inline));
inline void write_char_unsafe(char c) __attribute__((always_inline));
inline void write_char(char c) __attribute__((always_inline));
inline void write_int(int x) __attribute__((always_inline));
inline void write_ll(long long x) __attribute__((always_inline));
inline void write_uint(int x) __attribute__((always_inline));
inline void write_ull(long long x) __attribute__((always_inline));
void initialize() __attribute__((constructor));

char input_buffer[input_buffer_size], *p1 = input_buffer, *p2 = input_buffer;
inline char get_char_safe() {
    return (p1 == p2 && (p2 = (p1 = input_buffer) + fread(input_buffer, 1, input_buffer_size, stdin), p1 == p2) ?
                EOF :
                *p1++);
}
inline char get_char_unsafe() { return *p1++; }
inline char get_char() { return get_char_unsafe(); }

inline int read_int() {
    int res = 0;
    char ch = get_char();
    char f = 1;
    for (; unsigned(ch - '0') >= unsigned(10); ch = get_char()) f &= ch != '-';
    for (; unsigned(ch - '0') < unsigned(10); ch = get_char()) res = res * 10 + (ch ^ 48);
    return f ? res : -res;
}
inline long long read_ll() {
    long long res = 0;
    char ch = get_char();
    char f = 1;
    for (; unsigned(ch - '0') >= unsigned(10); ch = get_char()) f &= ch != '-';
    for (; unsigned(ch - '0') < unsigned(10); ch = get_char()) res = res * 10 + (ch ^ 48);
    return f ? res : -res;
}
inline int read_uint() {
    int res = 0;
    char ch = get_char();
    for (; unsigned(ch - '0') >= unsigned(10); ch = get_char()) {}
    for (; unsigned(ch - '0') < unsigned(10); ch = get_char()) res = res * 10 + (ch ^ 48);
    return res;
}
inline long long read_ull() {
    long long res = 0;
    char ch = get_char();
    for (; unsigned(ch - '0') >= unsigned(10); ch = get_char()) {}
    for (; unsigned(ch - '0') < unsigned(10); ch = get_char()) res = res * 10 + (ch ^ 48);
    return res;
}
inline void read_int(int& res) {
    char ch = get_char();
    char f = 1;
    for (; unsigned(ch - '0') >= unsigned(10); ch = get_char()) f &= ch != '-';
    for (; unsigned(ch - '0') < unsigned(10); ch = get_char()) res = res * 10 + (ch ^ 48);
    if (!f) res = -res;
}
inline void read_ll(long long& res) {
    char ch = get_char();
    char f = 1;
    for (; unsigned(ch - '0') >= unsigned(10); ch = get_char()) f &= ch != '-';
    for (; unsigned(ch - '0') < unsigned(10); ch = get_char()) res = res * 10 + (ch ^ 48);
    if (!f) res = -res;
}
inline void read_uint(int& res) {
    char ch = get_char();
    for (; unsigned(ch - '0') >= unsigned(10); ch = get_char()) {}
    for (; unsigned(ch - '0') < unsigned(10); ch = get_char()) res = res * 10 + (ch ^ 48);
}
inline void read_ull(long long& res) {
    char ch = get_char();
    for (; unsigned(ch - '0') >= unsigned(10); ch = get_char()) {}
    for (; unsigned(ch - '0') < unsigned(10); ch = get_char()) res = res * 10 + (ch ^ 48);
}

char output_buffer[output_buffer_size];
int pos;
inline void flush_output() { fwrite(output_buffer, 1, exchange(pos, 0), stdout); }
inline void write_char_safe(char c) { if (pos == output_buffer_size) { flush_output(); } output_buffer[pos++] = c; }
inline void write_char_unsafe(char c) { output_buffer[pos++] = c; }
inline void write_char(char c) { return write_char_unsafe(c); }

inline void write_int(int x) {
    static char number_buffer[32];
    if (x < 0) { write_char('-'); x *= -1; }
    int len = 0;
    for (; x >= 10; x /= 10) { number_buffer[len++] = (char)('0' + (x % 10)); }
    write_char((char)('0' + x));
    while (len) { write_char(number_buffer[--len]); }
}
inline void write_ll(long long x) {
    static char number_buffer[32];
    if (x < 0) { write_char('-'); x *= -1; }
    int len = 0;
    for (; x >= 10; x /= 10) { number_buffer[len++] = (char)('0' + (x % 10)); }
    write_char((char)('0' + x));
    while (len) { write_char(number_buffer[--len]); }
}
inline void write_uint(int x) {
    static char number_buffer[32];
    int len = 0;
    for (; x >= 10; x /= 10) { number_buffer[len++] = (char)('0' + (x % 10)); }
    write_char((char)('0' + x));
    while (len) { write_char(number_buffer[--len]); }
}
inline void write_ull(long long x) {
    static char number_buffer[32];
    int len = 0;
    for (; x >= 10; x /= 10) { number_buffer[len++] = (char)('0' + (x % 10)); }
    write_char((char)('0' + x));
    while (len) { write_char(number_buffer[--len]); }
}
void initialize() {
    p2 = (p1 = input_buffer) + fread(input_buffer, 1, input_buffer_size, stdin);
    assert(!atexit(flush_output));
}

} // namespace fastio
