template <long long mod>
class Modular {
    using CModRef = const Modular&;

 private:
    int val;
    constexpr static int inv(int p, int q) {
        return (p > 1 ? q - 1ll * inv(q % p, p) * q / p : 1);
    }
    constexpr static int ipow(int p, int q) {
        int r = 1;
        while (q) {
            if (q & 1)
                r = 1ll * r * p % mod;
            p = 1ll * p * p % mod;
            q >>= 1;
        }
        return r;
    }

 public:
    constexpr Modular(): val() {}
    constexpr Modular(CModRef) = default;
    
    template <typename T>
    constexpr int norm(T v) {
        int res;
        if (-mod <= v && v < mod)
            res = static_cast<int>(v);
        else
            res = static_cast<int>(v % mod);
        if (res < 0)
            return -res;
        return res;
    }
    template <typename T>
    constexpr Modular(T v): val(norm(v)) {}

    template <typename T>
    explicit constexpr operator T() const {
        return static_cast<T>(val);
    }
    constexpr int operator()() const {
        return val;
    }

    constexpr Modular operator-() const {
        return Modular(mod - val);
    }
    constexpr Modular& operator+=(CModRef other) {
        if ((val += other.val) >= mod)
            val -= mod;
        return *this;
    }
    constexpr Modular& operator-=(CModRef other) {
        if ((val -= other.val) < 0)
            val += mod;
        return *this;
    }
    constexpr Modular& operator*=(CModRef other) {
        val = 1ll * val * other.val % mod;
        return *this;
    }

    constexpr Modular inv() const {
        return Modular(inv(val, mod));
    }
    constexpr Modular& operator/=(CModRef other) {
        return *this *= other.inv();
    }

    constexpr Modular pow(long long k) const {
        return Modular(ipow(val, k % (mod - 1)));
    }

    friend constexpr Modular operator+(CModRef lhs, CModRef rhs) {
        Modular res = lhs;
        res += rhs;
        return res;
    }
    friend constexpr Modular operator-(CModRef lhs, CModRef rhs) {
        Modular res = lhs;
        res -= rhs;
        return res;
    }
    friend constexpr Modular operator*(CModRef lhs, CModRef rhs) {
        Modular res = lhs;
        res *= rhs;
        return res;
    }
    friend constexpr Modular operator/(CModRef lhs, CModRef rhs) {
        Modular res = lhs;
        res /= rhs;
        return res;
    }

    friend constexpr bool operator==(CModRef lhs, CModRef rhs) {
        return lhs() == rhs();
    }
    friend constexpr bool operator!=(CModRef lhs, CModRef rhs) {
        return lhs() != rhs();
    }

    friend constexpr istream &operator>>(istream& is, Modular& a) {
        long long v;
        is >> v;
        a = Modular(v);
        return is;
    }
    friend constexpr ostream &operator<<(ostream& os, CModRef a) {
        return os << a();
    }
};

const int md = 998244353;
// const int md = 1000000007;
using Mint = Modular<md>;

template <typename T>
struct Combinatorial {
    vector<T> fact, ifact;

    void clear(int n) {
        fact.resize(n + 1);
        ifact.resize(n + 1);
        fact[0] = 1;
        for (int i = 1; i <= n; ++i)
            fact[i] = fact[i - 1] * static_cast<T>(i);
        ifact[n] = fact[n].inv();
        for (int i = n; i > 0; --i)
            ifact[i - 1] = ifact[i] * static_cast<T>(i);
    }
    Combinatorial(int n = 0) {
        clear(n);
    }

    T C(T n, T r) const {
        return fact[n()] * ifact[r()] * ifact[(n - r)()];
    }
    T CR(T n, T r) const {
        return fact[(n + r - 1)()] * ifact[r()] * ifact[(n - 1)()];
    }
    T A(T n, T r) const {
        return fact[n()] * ifact[r()];
    }
    T AR(T n, long long r) const {
        return n.pow(r);
    }
    T P(T n) const {
        return fact[n()];
    }
    T PR(T n) const {
        return n.pow(n());
    }
    T CP(T n) const {
        return fact[(n - 1)()];
    }
};
