template <typename T>
struct Matrix {
    int n, m;
    vector<vector<T>> v;

    void clear(int nv, int mv) {
        n = nv, m = mv;
        v.assign(nv, vector<T>(mv, T()));
    }
    Matrix() {
        clear(0, 0);
    }
    Matrix(int nv, int mv) {
        clear(nv, mv);
    }

    Matrix transpose() const {
        Matrix<T> res(m, n);
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                res[j][i] = v[i][j];
            }
        }
        return res;
    }

    Matrix& operator+=(const Matrix& other) {
        assert(n == other.n && m == other.m);
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                v[i][j] += other.v[i][j];
            }
        }
        return *this;
    }
    Matrix operator-() {
        Matrix<T> res(n, m);
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                res.v[i][j] = -v[i][j];
            }
        }
        return res;
    }
    Matrix& operator-=(const Matrix& other) {
        return *this += -other;
    }
    Matrix& operator*=(T other) {
        for (auto& e : v)
            for (auto& u : e)
                u *= other;
        return *this;
    }

    friend Matrix<T> operator+(const Matrix<T>& lhs, const Matrix<T>& rhs) {
        auto res = lhs;
        return res += rhs;
    }
    friend Matrix<T> operator-(const Matrix<T>& lhs, const Matrix<T>& rhs) {
        auto res = lhs;
        return res -= rhs;
    }
    friend Matrix<T> operator*(const Matrix<T>& lhs, const Matrix<T>& rhs) {
        assert(lhs.m == rhs.n);
        Matrix<T> t = rhs.transpose(), res = Matrix(lhs.n, rhs.m);
        for (int k = 0; k < lhs.m; ++k) {
            for (int i = 0; i < lhs.n; ++i) {
                for (int j = 0; j < rhs.m; ++j) {
                    res.v[i][j] += lhs.v[i][k] * rhs.v[k][j];
                }
            }
        }
        return res;
    }

    Matrix power(long long val) {
        assert(n == m);
        Matrix res(n, n), p = *this;
        for (int i = 0; i < n; ++i)
            res[i][i] = 1;
        while (val) {
            if (val & 1) {
                res = res * p;
            }
            p = p * p;
            val >>= 1ll;
        }
        return res;
    }

    vector<T>& operator[](int x) {
        return v[x];
    }
};
