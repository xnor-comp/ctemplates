struct Sieve {
    vector<int> lp, pr;
    
    constexpr void build(int maxp) {
        lp.assign(maxp + 1, 0);
        pr.clear();

        for (int i = 2; i <= maxp; ++i) {
            if (!lp[i]) {
                lp[i] = i;
                pr.push_back(i);
            }
            for (int j = 0; i * pr[j] <= maxp; ++j) {
                lp[i * pr[j]] = pr[j];
                if (pr[j] == lp[i]) {
                    break;
                }
            }
        }
    }
    Sieve() = default;
    Sieve(int maxp) {
        build(maxp);
    }

    int is_prime(int v) const {
        return lp[v] == v;
    }
    int low(int v) const {
        return lp[v];
    }
    vector<int> primes() const {
        return pr;
    }
    vector<int> fact(int v) const {
        vector<int> res;
        while (v != 1)
            res.push_back(lp[v]), v /= lp[v];
        return res;
    }
};
