int mpow(int p, int q, int m) {
    int res = 1;
    p %= m;
    q %= m - 1;
    while (q) {
        if (q & 1)
            res = 1ll * p * res % m;
        p = 1ll * p * p % m;
        q >>= 1;
    }
    return res;
}
int minv(int p, int q) {
    return (p > 1 ? q - 1ll * minv(q % p, p) * q / p : 1);
}

long long mpow(long long p, long long q, long long m) {
    long long res = 1;
    p %= m;
    q %= m - 1;
    while (q) {
        if (q & 1)
            res = p * res % m;
        p = p * p % m;
        q >>= 1;
    }
    return res;
}
long long minv(long long p, long long q) {
    return (p > 1 ? q - minv(q % p, p) * q / p : 1);
}
