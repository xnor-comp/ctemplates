using ull = unsigned long long;
constexpr ull mrbase[] = {2, 325, 9375, 28178, 450775, 9780504, 1795265022};
ull modmul(ull p, ull q, ull m) {
    long long res = p * q - m * ull(1.L / m * p * q);
    return res + m * (res < 0) - m * (res >= (long long)m);
}
ull modpow(ull p, ull q, ull m) {
    ull ans = 1;
    while (q) {
        if (q & 1) ans = modmul(ans, p, m);
        p = modmul(p, p, m);
        q >>= 1;
    }
    return ans;
}
bool miller_rabin(ull n) {
    if (n < 2 || n % 6 % 4 != 1) return (n | 1) == 3;
    ull s = __builtin_ctzll(n - 1), d = n >> s;
    for (auto e : mrbase) {
        ull p = modpow(e % n, d, n), i = s;
        while (p != 1 && p != n - 1 && e % n && i--)
            p = modmul(p, p, n);
        if (p != n - 1 && i != s)
            return 0;
    }
    return 1;
}
