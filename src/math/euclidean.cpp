template <typename T>
struct EuclideanResult {
    T a, b, r;
};
template <typename T>
struct DiophantineResult {
    T a, b;
    bool r;
};
template <typename T>
EuclideanResult<T> extended_euclidean(T a, T b) {
    EuclideanResult<T> x(1, 0, a), y(0, 1, b);
    while (y.r > 0) {
        long long k = x.r / y.r;
        x.a -= k * y.a;
        x.b -= k * y.b;
        x.r -= k * y.r;
        swap(x, y);
    }
    return x;
}
template <typename T>
DiophantineResult<T> solve_diophantine(T a, T b, T c) {
    if ((a | b) == 0)
        return DiophantineResult<T>(0, 0, !c);
    DiophantineResult<T> ans(0, 0, 0);
    auto res = extended_euclidean(a, b);
    if (c % res.r != 0)
        return ans;
    ans.r = 1;
    ans.a = res.a * 1ll * c / res.r;
    ans.b = res.b * 1ll * c / res.r;
    T ma = b / res.r;
    T mb = a / res.r;
    if (ans.a < 0) {
        T k = (ma - ans.a - 1) / ma;
        ans.a += k * ma;
        ans.b -= k * mb;
    } else {
        T k = ans.a / ma;
        ans.a -= k * ma;
        ans.b += k * mb;
    }
    return ans;
}
