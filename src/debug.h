#ifndef _DBG
#define _DBG

template <typename T>
void printv(T vec) {
    cerr << "{";
    auto bg = begin(vec), ed = end(vec);
    for (auto it = bg; it != ed; it++)
        cerr << (it != bg ? ", " : "") << *it;
    cerr << "}";
}
template <typename T>
void printvv(T vec) {
    cerr << "{";
    auto bg = begin(vec), ed = end(vec);
    for (auto it = bg; it != ed; it++)
        cerr << (it != bg ? ", \n " : ""), printv(*it);
    cerr << "}\n";
}
#define dbg(x) (cerr << __PRETTY_FUNCTION__ << ": " << __LINE__ << "; " << #x << " = " << x << '\n')
#define dv(x) (cerr << __PRETTY_FUNCTION__ << ": " << __LINE__ << "; " << #x << ":\n", printv(x), cerr << '\n')
#define dvv(x) (cerr << __PRETTY_FUNCTION__ << ": " << __LINE__ << "; " << #x << ":\n", printvv(x))

#endif
