template <typename T = long long>
struct BIT {
    int n;
    vector<T> bit;

    void clear(int sz) {
        n = sz;
        bit.assign(n + 1, 0);
    }
    BIT(int sz = 0) {
        clear(sz);
    }

    void add(int i, T x) {
        for (++i; i <= n; i += i & -i) bit[i] += x;
    }
    T query(int i) const {
        if (i < 0) return 0;
        T res = 0;
        for (++i; i > 0; i &= i - 1) res += bit[i];
        return res;
    }
    T query(int l, int r) const {
        return query(r) - query(l - 1);
    }
};
