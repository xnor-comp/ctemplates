struct UnionFind {
    vector<int> e;
    void clear(int n) { e.assign(n, -1); }
    UnionFind(int n = 0) { clear(n); }
    int find(int x) { return e[x] < 0 ? x : e[x] = find(e[x]); }
    int size(int x) { return -e[find(x)]; }
    bool same(int x, int y) { return find(x) == find(y); }
    bool join(int a, int b) {
        a = find(a), b = find(b);
        if (a == b) return 0;
        if (e[a] > e[b]) swap(a, b);
        return e[a] += exchange(e[b], a);
    }
};
