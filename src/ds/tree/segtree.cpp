template <typename Node>
struct SegTree {
    int n;
    vector<Node> tr;

    void clear(int sz) {
        int f = 1;
        while (f < sz)
            f <<= 1;
        n = f;
        tr.assign(f << 1, Node());
    }
    SegTree() = default;
    SegTree(const SegTree&) = default;
    SegTree(int sz) {
        clear(sz);
    }

    template <typename T>
    void build(int sz, const T& a) {
        for (int i = n; i < n + sz; ++i) tr[i] = Node(a[i - n]);
        for (int i = n - 1; i > 0; --i) tr[i] = tr[i << 1] + tr[i << 1 ^ 1];
    }
    void modify(int x, Node k) {
        for (tr[x += n] = k; x > 1; x >>= 1) tr[x >> 1] = tr[x] + tr[x ^ 1];
    }
    Node query(int l, int r) const {
        Node res;
        for (l += n, r += n + 1; l < r; l >>= 1, r >>= 1) {
            if (l & 1) res = res + tr[l++];
            if (r & 1) res = res + tr[--r];
        }
        return res;
    }
};
