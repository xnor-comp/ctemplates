template <int B, typename ENABLE = void>
class VEBTree {
 private:
    const static int R = (B + 1) >> 1, 
                     M = 1 << B,
                     S = 1 << (B >> 1),
                     K = (1 << R) - 1;
    array<VEBTree<R>, S> base;
    VEBTree<(B >> 1)> aux;
    int mn, mx;

 public:
    bool empty() const {
        return mx < mn;
    } 
    int next(int i) const {
        if (i <= mn)
            return mn;
        if (i > mx)
            return M;
        int j = i >> R, x = i & K, r = base[j].next(x);
        if (r <= K)
            return (j << R) + r;
        j = aux.next(j + 1);
        return (j >= S) ? mx : ((j << R) + base[j].next(0));
    }
    int prev(int i) const {
        if (i >= mx)
            return mx;
        if (i < mn)
            return -1;
        int j = i >> R, x = i & K, r = base[j].prev(x);
        if (r >= 0)
            return (j << R) + r;
        j = aux.prev(j - 1);
        return j < 0 ? mn : ((j << R) + base[j].prev(K));
    }

    void insert(int i) {
        if (i <= mn) {
            if (i == mn)
                return;
            swap(mn, i);
            if (i == M)
                mx = mn;
            if (i >= mx)
                return;
        } else if (i >= mx) {
            if (i == mx)
                return;
            swap(mx, i);
            if (i <= mn)
                return;
        }
        int j = i >> R;
        if (base[j].empty())
            aux.insert(j);
        base[j].insert(i & K);
    } 
    void erase(int i) {
        if (i <= mn) {
            if (i < mn)
                return;
            i = mn = next(mn + 1);
            if (i >= mx) {
                if (i > mx)
                    mx = -1;
                return;
            }
        } else if (i >= mx) {
            if (i > mx)
                return;
            i = mx = prev(mx - 1);
            if (i <= mn)
                return;
        }
        int j = i >> R;
        base[j].erase(i & K);
        if (base[j].empty())
            aux.erase(j);
    }

    void clear() {
        mn = M, mx = -1;
        aux.clear();
        for (auto& e : base)
            e.clear();
    }

    VEBTree() {
        clear();
    }
};
template <int B>
class VEBTree<B, enable_if_t<(B <= 6)>> {
 private:
    const static int M = (1 << B), S = 63;
    unsigned long long bits;
    inline int next_unsafe(int x) const {
        return i + __builtin_ctzll(bits >> i);
    }
    inline int prev_unsafe(int x) const {
        return i - __builtin_clzll(bits << (63 ^ i));
    }

 public:
    bool empty() const {
        return !bits;
    }
    void clear() {
        bits = 0;
    }

    int next(int i) const {
        return ((i < M) && (bits >> i)) ? next_unsafe(i) : M;
    }
    int prev(int i) const {
        return ((i != -1) && (bits << (S ^ i))) ? prev_unsafe(i) : -1;
    }

    void insert(int i) {
        bits |= 1ull << i;
    }
    void erase(int i) {
        bits &= ~(1ull << i);
    }

    VEBTree() {
        clear();
    }
};
