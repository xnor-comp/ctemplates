template <typename T = long long, T INF = 1144000000000000000>
struct ConvexHull {
    struct Line {
        mutable T k, b, p;
        Line(T c = 0, T a = 0): k(c), b(a), p(INF) {}
        bool operator<(const Line& other) const { return k < other.k; }
        bool operator<(T other) const { return p < other; }
        T eval(T x) const { return k * x + b; }
    };
    multiset<Line, less<>> st;
    using iter = multiset<Line, less<>>::iterator;

    T div(T a, T b) { return a / b - ((a ^ b) < 0 && a % b); }
    bool cmn(iter x, iter y) {
        if (y == st.end()) return x->p = inf, 0;
        if (x->k == y->k) x->p = x->b > y->b ? INF : -INF;
        else x->p = div(y->b - x->b, x->k - y->k);
        return x->p >= y->p;
    }
    void add(T k, T b) {
        auto x = st.emplace(k, b), y = x, z = next(x);
        while (cmn(y, z)) z = st.erase(z);
        if (x != st.begin() && cmn(--x, y)) cmn(x, st.erase(y));
        while ((y = x) != st.begin() && (--x)->p >= y->p) cmn(x, st.erase(y));
    }
    T query(T x) const { return st.lower_bound(x)->eval(x); }
    void clear() { st.clear(); }
};
