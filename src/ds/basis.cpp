template <typename T, int B>
struct Basis {
    T sz = 0;
    array<T, B> b;

    void clear() {
        sz = 0;
        fill(b.begin(), b.end(), 0);
    }
    Basis() {
        clear();
    }

    void add(T x) {
        for (T i = B - 1; i >= 0; i--) {
            if (~x >> i & T(1)) continue;
            if (!b[i]) return b[i] = x, void(sz++);
            x ^= b[i];
        }
    }

    T max() const {
        T x = 0;
        for (T i = B - 1; i >= 0; --i)
            if (~x >> i & T(1))
                x ^= b[i];
        return x;
    }
    T nth_element(T n) const {
        T x = 0, tot = 1 << sz;
        for (T i = B - 1; i >= 0; --i)
            if (b[i]) {
                T l = tot >> 1;
                bool ok = x >> i & T(1);
                if ((l < n && !ok) || (l >= n && ok)) x ^= b[i];
                if (l < n) n -= l;
                tot >>= 1;
            }
        return x;
    }

    bool contains(T n) const {
        for (int i = B - 1; i >= 0; n ^= b[i--]) {
            if (~n >> i & 1) continue;
            if (!b[i]) return 0;
            n ^= b[i];
        }
        return 1;
    }
    T size() const {
        return T(1) << sz;
    }
};
