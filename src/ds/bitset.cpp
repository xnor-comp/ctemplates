template <int BITS>
class Bitset {
    using ull = unsigned long long;

 private:
    constexpr static ull lower_half  = (1ull << 32) - 1,
                         upper_half  = ~lower_half,
                         all_set     = ~0ull;
    constexpr static int block_count = (BITS + 63) >> 6;

    array<ull, block_count> bs;

    inline static constexpr ull get_bit(int i, ull mask = 1) __attribute__((always_inline)) {
        return mask << (i & 63);
    }

 public:
    void set() {
        for (auto& e : bs)
            e = all_set;
    }
    void clear() {
        for (auto& e : bs)
            e = 0;
    }
    void flip() {
        for (auto& e : bs)
            e = ~e;
    }

    Bitset(bool tp = 0) {
        if (tp)
            set();
        else
            clear();
    }
    inline bool get(unsigned int i) const __attribute__((always_inline)) {
        return bs[i >> 6] & get_bit(i);
    }
    inline void set(unsigned int i, ull k) __attribute__((always_inline)) {
        bs[i >> 6] = (bs[i >> 6] & ~get_bit(i)) | get_bit(i, k);
    }
    inline void set0(unsigned int i) __attribute__((always_inline)) {
        bs[i >> 6] &= ~get_bit(i);
    }
    inline void set1(unsigned int i) __attribute__((always_inline)) {
        bs[i >> 6] |= get_bit(i);
    }
    inline void flip(unsigned int i) __attribute__((always_inline)) {
        bs[i >> 6] ^= get_bit(i);
    }

    int next(int i = 0) const {
        int j = i >> 6;
        ull tmp = bs[j] >> (i & 63);
        if (tmp)
            return min(BITS, i + __builtin_ctzll(tmp));
        for (++j; (j << 6) < BITS; ++j)
            if (bs[j])
                return min(BITS, (j << 6) + __builtin_ctzll(bs[j]));
        return BITS;
    }
    int prev(int i = block_count - 1) const {
        int j = i >> 6;
        ull tmp = bs[j] << (63 ^ (i & 63));
        if (tmp)
            return max(0, i + 1 - __builtin_clzll(tmp)) - 1;
        for (--j; ((j + 1) << 6) > 0; --j)
            if (bs[j])
                return max(0, ((j + 1) << 6) - __builtin_clzll(bs[j])) - 1;
        return -1;
    }

    unsigned int count() const {
        unsigned int res{};
        for (int i = 0; i < block_count - 1; ++i)
            res += __builtin_popcountll(bs[i]);
        return res + __builtin_popcountll((1 << (BITS & 63)) - 1 & bs.back());
    }
    unsigned int count(unsigned int l, unsigned int r) const {
        int res{};
        if (l & 63)
            res -= __builtin_popcountll(bs[l >> 6] << (64 - (l & 63)));
        if ((r + 1) & 63)
            res -= __builtin_popcountll(bs[r >> 6] >> ((r + 1) & 63));
        for (int j = (l >> 6); j <= (r >> 6); ++j)
            res += __builtin_popcountll(bs[j]);
        return res;
    }

    Bitset operator~() const {
        Bitset<BITS> res = *this;
        return res.flip();
    }

    Bitset operator|(const Bitset& other) const {
        Bitset<BITS> res(1);
        for (int i = 0; i < block_count; ++i)
            res.bs[i] = bs[i] | other.bs[i];
        return res;
    }
    Bitset& operator|=(const Bitset& other) {
        for (int i = 0; i < block_count; ++i)
            bs[i] |= other.bs[i];
        return *this;
    }
    Bitset operator^(const Bitset& other) const {
        Bitset<BITS> res;
        for (int i = 0; i < block_count; ++i)
            res.bs[i] = bs[i] ^ other.bs[i];
        return res;
    }
    Bitset& operator^=(const Bitset& other) {
        for (int i = 0; i < block_count; ++i)
            bs[i] ^= other.bs[i];
        return *this;
    }
    Bitset operator&(const Bitset& other) const {
        Bitset<BITS> res;
        for (int i = 0; i < block_count; ++i)
            res.bs[i] = bs[i] & other.bs[i];
        return res;
    }
    Bitset& operator&=(const Bitset& other) {
        for (int i = 0; i < block_count; ++i)
            bs[i] &= other.bs[i];
        return *this;
    }

    Bitset shift_left(int d) const {
        Bitset<BITS> res;
        int s = min(d >> 6, block_count), r = d & 63;
        if (r == 0) {
            for (int i = s; i < block_count; ++i)
                res.bs[i] = bs[i - s];
        } else {
            if (s < block_count)
                res.bs[s] = bs[0] << r;
            for (int i = s + 1; i < block_count; ++i)
                res.bs[i] = (bs[i - s] << r) | (bs[i - s - 1] >> (64 - r));
        }
        return res;
    }
    Bitset shift_right(int d) const {
        Bitset<BITS> res;
        int s = min(d >> 6, block_count), r = d & 63;
        if (r == 0) {
            for (int i = block_count - s - 1; i >= 0; --i) {
                res.bs[i] = bs[i + s];
            }
        } else {
            if (s < block_count)
                res.bs[block_count - s - 1] = bs[block_count - 1] >> r;
            for (int i = block_count - s - 2; i >= 0; --i)
                res.bs[i] = (bs[i + s] >> r) | (bs[i + s + 1] << (64 - r));
        }
        return res;
    }
    Bitset operator<<(int d) const {
        return shift_left(d);
    }
    Bitset operator>>(int d) const {
        return shift_right(d);
    }

    Bitset& operator<<=(int d) {
        int s = min(d >> 6, block_count), r = d & 63;
        if (r == 0) {
            for (int i = block_count - 1; i >= s; --i)
                bs[i] |= bs[i - s];
        } else {
            for (int i = block_count - 1; i >= s + 1; --i)
                bs[i] |= (bs[i - s] << r) | (bs[i - s - 1] >> (64 - r));
            if (s < block_count)
                bs[s] |= bs[0] << r;
        }
        return *this;
    }
    Bitset& operator>>=(int d) {
        int s = min(d >> 6, block_count), r = d & 63;
        if (r == 0) {
            for (int i = block_count - s - 1; i >= 0; --i) {
                bs[i] |= bs[i + s];
            }
        } else {
            if (s < block_count)
                bs[block_count - s - 1] |= bs[block_count - 1] >> r;
            for (int i = block_count - s - 2; i >= 0; --i)
                bs[i] |= (bs[i + s] >> r) | (bs[i + s + 1] << (64 - r));
        }
        return *this;
    }
};
