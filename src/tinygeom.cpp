namespace bgeom {

using tp = long long;
using fl = long double;
constexpr fl eps = 1e-10;
const fl fl_inf = (fl)1 / 0.;

template<class T, typename std::enable_if<!std::is_integral<T>::value, int>::type = 0>
inline constexpr bool   is_equal(T x, T y) { return abs(x - y) < eps; }
template<class T, typename std::enable_if<!std::is_integral<T>::value, int>::type = 0>
inline constexpr bool   is_positive(T x) { return x > eps; }
template<class T, typename std::enable_if<!std::is_integral<T>::value, int>::type = 0>
inline constexpr bool   is_nonnegative(T x) { return x > -eps; }
template<class T, typename std::enable_if<!std::is_integral<T>::value, int>::type = 0>
inline constexpr int    sgn(T x) { return (x > eps) - (x < -eps); }

template<class T, typename std::enable_if<std::is_integral<T>::value, int>::type = 0>
inline constexpr bool   is_equal(T x, T y) { return x == y; }
template<class T, typename std::enable_if<std::is_integral<T>::value, int>::type = 0>
inline constexpr bool   is_positive(T x) { return x > 0; }
template<class T, typename std::enable_if<std::is_integral<T>::value, int>::type = 0>
inline constexpr bool   is_nonnegative(T x) { return x >= 0; }
template<class T, typename std::enable_if<std::is_integral<T>::value, int>::type = 0>
inline constexpr int    sgn(T x) { return (x > 0) - (x < 0); }

template <typename T> T sq(T f) { return f * f; }

class Vec {
 public:
    tp x, y;
    Vec(tp x_ = tp(), tp y_ = tp()): x(x_), y(y_) {}

    Vec operator+(const Vec& other) const {
        return Vec(x + other.x, y + other.y);
    }
    Vec operator-(const Vec& other) const {
        return Vec(x - other.x, y - other.y);
    }

    template <typename T>
    Vec operator*(T other) const {
        return Vec(x * other, y * other);
    }
    template <typename T>
    friend Vec operator*(T lhs, const Vec& rhs) {
        return rhs * lhs;
    }

    template <typename T>
    Vec operator/(T other) const {
        return Vec(x / other, y / other);
    }

    bool operator<(Vec other) const {
        return tie(x, y) < tie(other.x, other.y);
    }
    bool operator==(const Vec& other) const {
        return is_equal(x, other.x) && is_equal(y, other.y);
    }

    tp dot(const Vec& other) const {
        return x * other.x + y * other.y;
    }
    tp cross(const Vec& other) const {
        return x * other.y - y * other.x;
    }
    tp cross(const Vec& lhs, const Vec& rhs) const {
        return Vec(*this, lhs).cross(Vec(*this, rhs));
    }

    tp dist_sq() const {
        return dot(*this);
    }
    tp dx(const Vec& other) const {
        return other.x - x;
    }
    tp dy(const Vec& other) const {
        return other.y - y;
    }

    fl dist(const Vec& other) const {
        return sqrtl(Vec(*this, other).dist_sq());
    }
    fl len() const {
        return sqrtl(dist_sq());
    }

    fl angle() const {
        return atan2l(y, x);
    }
    fl angle(const Vec& other) const {
        return atan2l(cross(other), dot(other));
    }

    Vec unit() const {
        if constexpr (!std::is_integral<tp>::value) {
            assert("tp must be a non-integer type");
        }
        return *this / len();
    }
    Vec cc() const {
        return Vec(-y, x);
    }
    Vec cw() const {
        return Vec(y, -x);
    }

    bool collinear(const Vec& other) const {
        return is_equal(x * other.y, y * other.x);
    }
    bool codir(const Vec& other) const {
        return collinear(other) &&
               (sgn(x) == sgn(other.x)) &&
               (sgn(y) == sgn(other.y));
    }
    bool raycomp(const Vec& other, fl other_x, fl other_y) const {
        other_x -= x;
        other_y -= y;
        return is_equal(other.x * other_y, other.y * other_x) &&
               (sgn(other.x) == sgn(other_x)) &&
               (sgn(other.y) == sgn(other_y));
    }

    bool covers(const Vec& other) const {
        return *this == other;
    }

    Vec(const Vec& alpha, const Vec& beta) {
        *this = beta - alpha;
    }

    friend istream& operator>> (istream& in, Vec& v) {
        in >> v.x >> v.y;
        return in;
    }
    friend ostream& operator<< (ostream& out, const Vec& v) {
        cout << v.x << ' ' << v.y;
        return out;
    }
};

int side(Vec s, Vec p, Vec q) {
    return sgn(s.cross(p, q));
}

class Line {
 public:
    Vec a, b;
    Line(Vec x, Vec y): a(x), b(y) {}
    Line(fl ac, fl bc, fl cc) {
        if (sgn(bc)) {
            a.x = 0, b.x = 1;
            a.y = cc / bc;
            b.y = -(ac + cc) / bc;
        } else {
            a.y = 0, b.y = 1;
            a.x = cc / ac;
            b.x = -(bc + cc) / ac;
        }
    }

    bool covers(const Vec& v) const {
        return Vec(a, b).collinear(Vec(a, v));
    }
    bool parallel(const Line& other) const {
        return Vec(a, b).collinear(Vec(other.a, other.b));
    }

    fl dist(const Vec& other) const {
        return abs(fl(Vec(a, b).cross(Vec(a, other))) / a.dist(b));
    }
    fl dist(const Line& other) const {
        if (parallel(other)) {
            return dist(other.a);
        }
        return 0;
    }

    Vec get_norm() const {
        return Vec(a, b).cc();
    }

    bool operator==(const Line& other) const {
        return covers(other.a) && covers(other.b);
    }

    pair<int, Vec> intersect(const Line& other) const {
        if constexpr (!std::is_integral<tp>::value) {
           assert("tp must be a non-integer type");
        }
        if (parallel(other)) {
            return make_pair(2 * (*this == other), Vec());
        }
        tp d = Vec(a, b).cross(Vec(other.a, other.b));
        tp p = Vec(other.a).cross(b, other.b);
        tp q = Vec(other.a).cross(other.b, a);
        return {1, (a * p + b * q) / d};
    }

    Vec proj(const Vec& other) const {
        if constexpr (std::is_integral<tp>::value) {
            assert(0 && "tp must be a non-integer type");
        }
        return intersect(Line(other, other + get_norm())).second;
    }
};

bool sameside(Vec a, Vec b, Vec c, Vec d) {
    int x = side(a, b, c);
    int y = side(a, b, d);
    if (x >= 0 && y >= 0) {
        return true;
    }
    if (x <= 0 && y <= 0) {
        return true;
    }
    return false;
}

class Ray {
 public:
    Vec s, d;
    Ray(Vec a = Vec(), Vec b = Vec()): s(a), d(b - a) {}

    bool covers(const Vec& v) const {
        return s == v || (v - s).codir(d);
    }
    bool coversfl(fl other_x, fl other_y) const {
        return (is_equal(s.x * (fl)1, other_x) &&
                is_equal(s.y * (fl)1, other_y)) ||
                s.raycomp(d, other_x, other_y);
    }

    bool intersects(const Ray& other) const {
        Vec a = s, b = s + d, c = other.s, d = c + other.d;
        if (Line(a, b).parallel(Line(c, d))) {
            return covers(c) || other.covers(a);
        }
        fl d1 = Vec(a, b).cross(Vec(c, d));
        tp p = Vec(c).cross(b, d);
        tp q = Vec(c).cross(d, a);
        Vec f = (a * p + b * q);
        fl xx = f.x;
        fl yy = f.y;
        xx /= d1;
        yy /= d1;
        return coversfl(xx, yy) && other.coversfl(xx, yy);
    }

    fl dist(const Vec& other) const {
        if (sameside(s, s + d.cc(), other, s + d)) {
            return Line(s, s + d).dist(other);
        }
        return s.dist(other);
    }
    fl dist(const Ray& other) const {
        fl res = min(dist(other.s), other.dist(s));
        if (intersects(other)) {
            return 0;
        }
        return res;
    }
    fl dist(const Line& other) const {
        return min({Ray(s, s + d).dist(Ray(other.a, other.b)),
                    Ray(s, s + d).dist(Ray(other.b, other.a))});
    }
};

class Segment {
 public:
    Vec a, b;
    Segment(Vec x = Vec(), Vec y = Vec()): a(x), b(y) {}

    bool covers(const Vec& v) const {
        return Ray(a, b).covers(v) && Ray(b, a).covers(v);
    }

    fl dist(const Vec& other) const {
        return max(Ray(a, b).dist(other), Ray(b, a).dist(other));
    }
    fl dist(const Line& other) const {
        return max(Ray(a, b).dist(other), Ray(b, a).dist(other));
    }
    fl dist(const Ray& other) const {
        return max(Ray(a, b).dist(other), Ray(b, a).dist(other));
    }
    fl dist(const Segment& other) const {
        return max({Ray(a, b).dist(Ray(other.a, other.b)),
                    Ray(a, b).dist(Ray(other.b, other.a)),
                    Ray(b, a).dist(Ray(other.a, other.b)),
                    Ray(b, a).dist(Ray(other.b, other.a))});
    }
};

class Circle {
 public:
    Vec c;
    tp r;
    Circle(Vec a = Vec(), tp b = tp()): c(a), r(b) {}

    int intersect(const Circle& other, vector<Vec>& pts) const {
        if (c == other.c) {
            if (is_equal(r, other.r)) {
                return 3;
            }
            return 0;
        }
        Vec v = other.c - c;
        fl d2 = v.dist_sq(), s = r + other.r, d = r - other.r;
        fl p = (d2 + sq(r) - sq(other.r)) / (d2 * 2), h2 = sq(r) - sq(p) * d2;
        if (is_positive(d2 - sq(s)) || is_positive(sq(d) - d2)) {
            return 0;
        }
        Vec mi = c + v * p, per = v.cc() * sqrtl(max(fl(0), h2) / d2);
        pts = {mi - per, mi + per};
        if (!is_positive(h2)) {
            pts.pop_back();
        }
        return pts.size();
    }
    int intersect(const Line& other, vector<Vec>& pts) const {
        Vec v(other.a, other.b);
        Vec p = other.a + v * (c - other.a).dot(v) / v.dist_sq();
        fl s = other.a.cross(other.b, c);
        fl h2 = sq(r) - sq(s) / v.dist_sq();
        if (!is_nonnegative(h2)) {
            return 0;
        }
        if (!sgn(h2)) {
            pts.push_back(p);
            return 1;
        }
        Vec h = v.unit() * sqrt(h2);
        pts = {p - h, p + h};
        return 2;
    }

    int tangents(const Circle& other, vector<pair<Vec, Vec>>& pts) const {
        Vec d(c, other.c);
        fl dr = r - other.r;
        fl d2 = d.dist_sq();
        fl h2 = d2 - sq(dr);
        if (!sgn(d2) || !is_nonnegative(h2)) {
            return 0;
        }
        for (double sign : {-1, 1}) {
            Vec v = (d * dr + d.cc() * sqrt(h2) * sign) / d2;
            pts.emplace_back(c + v * r, other.c + v * other.r);
        }
        if (!sgn(h2)) {
            pts.pop_back();
        }
        return pts.size();
    }

    fl dist(const Vec& other) const {
        return max(fl(0), c.dist(other) - r);
    }
    fl dist(const Line& other) const {
        return max(fl(0), other.dist(c) - r);
    }
    fl dist(const Ray& other) const {
        return max(fl(0), other.dist(c) - r);
    }
    fl dist(const Segment& other) const {
        return max(fl(0), other.dist(c) - r);
    }
    fl dist(const Circle& other) const {
        return max(fl(0), other.c.dist(c) - r - other.r);
    }
};

fl area(const vector<Vec>& v) {
    fl a = v.back().cross(v[0]);
    for (int i = 0; i < (int)v.size() - 1; ++i) {
        a += v[i].cross(v[i + 1]);
    }
    return a / 2.;
}

fl perimeter(const vector<Vec>& v) {
    fl ans = 0;
    for (int i = 1; i <= (int)v.size(); ++i) {
        ans += v[i % v.size()].dist(v[i - 1]);
    }
    return ans;
}

bool inside_hull(const vector<Vec>& l, const Vec& p, bool strict = 0) {
    if (l.size() == 1) {
        return p == l[0];
    }
    if (l.size() == 2) {
        return Segment(l[0], l[1]).covers(p);
    }
    int a = 1, b = l.size() - 1, r = !strict;
    if (side(l[0], l[a], l[b]) > 0) {
        swap(a, b);
    }
    if (side(l[0], l[a], p) >= r || side(l[0], l[b], p) <= -r) {
        return 0;
    }
    while (abs(a - b) > 1) {
        (side(l[0], l[(a + b) / 2], p) > 0 ? b : a) = (a + b) / 2;
    }
    return sgn(l[a].cross(l[b], p)) < r;
}

vector<Vec> convex_hull(vector<Vec> pts) {
    if (pts.size() <= 1) {
        return pts;
    }
    sort(pts.begin(), pts.end());
    vector<Vec> h(pts.size() + 1);
    int s{}, t{};
    for (int it = 2; it--; s = --t, reverse(pts.begin(), pts.end())) {
        for (auto e : pts) {
            while (t >= s + 2 &&
                   !is_positive(h[t - 2].cross(h[t - 1], e))) {
                t--;
            }
            h[t++] = e;
        }
    }
    return {h.begin(), h.begin() + t - (t == 2 && h[0] == h[1])};
}

pair<Vec, Vec> diameter(const vector<Vec>& pts) {
    int n = pts.size(), j = n >= 2;
    pair<tp, pair<Vec, Vec>> res({0, {pts[0], pts[0]}});
    for (int i = 0; i < j; ++i) {
        for (;; j = (j + 1) % n) {
            res = max(res, {Vec(pts[i], pts[j]).dist_sq(), {pts[i], pts[j]}});
            if (is_nonnegative((pts[(j + 1) % n] - pts[j]).cross(pts[i + 1] - pts[i]))) {
                break;
            }
        }
    }
    return res.second;
}

int is_cmp(int i, int j, const vector<Vec>& pts, const Vec& dir) {
    return sgn(dir.cc().cross(Vec(pts[j % pts.size()], pts[i % pts.size()])));
}
bool is_extr(int i, const vector<Vec>& pts, const Vec& dir) {
    return is_cmp(i + 1, i, pts, dir) != -1 && is_cmp(i, i - 1 + pts.size(), pts, dir) == -1;
}
fl is_cmpl(int i, const vector<Vec>& pts, const Vec& a, const Vec& b) {
    return a.cross(pts[i], b);
}
int is_extrn(const vector<Vec>& pts, const Vec& dir) {
    int n = pts.size(), lo = 0, hi = n;
    if (is_extr(0, pts, dir)) {
        return 0;
    }
    while (lo + 1 < hi) {
        int m = (lo + hi) / 2;
        if (is_extr(m, pts, dir)) {
            return m;
        }
        int ls = is_cmp(lo + 1, lo, pts, dir), ms = is_cmp(m + 1, m, pts, dir);
        (ls < ms || (ls == ms && ls == sgn(is_cmp(lo, m, pts, dir))) ? hi : lo) = m;
    }
    return lo;
}
bool intersect(const vector<Vec>& pts, const Line& l) {
    int ea = is_extrn(pts, Vec(l.b, l.a).cc());
    int eb = is_extrn(pts, Vec(l.a, l.b).cc());
    return sgn(is_cmpl(ea, pts, l.a, l.b)) >= 0 &&
           sgn(is_cmpl(eb, pts, l.a, l.b)) <= 0;
}

void mk_rot(vector<Vec>& p) {
    int idx = 0;
    for (int i = 1; i < (int)p.size(); ++i) {
        if (p[i].y < p[idx].y || (is_equal(p[i].y, p[idx].y) && p[i].x < p[idx].x)) {
            idx = i;
        }
    }
    rotate(p.begin(), p.begin() + idx, p.end());
}
vector<Vec> minkowski_sum(vector<Vec> a, vector<Vec> b) {
    mk_rot(a);
    mk_rot(b);
    a.push_back(a[0]);
    a.push_back(a[1]);
    b.push_back(b[0]);
    b.push_back(b[1]);
    vector<Vec> res;
    int i = 0, j = 0;
    while (i < (int)a.size() - 2 || j < (int)b.size() - 2) {
        res.push_back(a[i] + b[j]);
        fl prod = Vec(a[i], a[i + 1]).cross(Vec(b[j], b[j + 1]));
        if (is_nonnegative(prod)) {
            ++i;
        }
        if (!is_positive(prod)) {
            ++j;
        }
    }
    return res;
}

fl dist(vector<Vec> a, vector<Vec> b) {
    fl res = fl_inf;
    if (a.size() < b.size()) {
        swap(a, b);
    }
    if (b.size() <= 2) {
        if (b.size() == 1) {
            if (inside_hull(a, b[0])) {
                return 0;
            }
            if (a.size() == 1) {
                return a[0].dist(b[0]);
            } else {
                for (int i = 1; i <= (int)a.size(); ++i) {
                    res = min(res, Segment(a[i % a.size()], a[i - 1]).dist(b[0]));
                }
            }
        } else {
            if (inside_hull(a, b[0]) || inside_hull(a, b[1])) {
                return 0;
            }
            for (int i = 1; i <= (int)a.size(); ++i) {
                res = min(res, Segment(a[i % a.size()], a[i - 1]).dist(Segment(b[0], b[1])));
            }
        }
        return res;
    }
    vector<Vec> neg(b.begin(), b.end());
    for (auto& [x, y] : neg) {
        x = -x, y = -y;
    }
    auto sm = minkowski_sum(a, neg);
    if (inside_hull(sm, Vec())) {
        return 0;
    }
    sm.push_back(sm[0]);
    for (int i = 0; i < (int)sm.size() - 1; ++i) {
        res = min(res, Segment(sm[i], sm[i + 1]).dist(Vec()));
    }
    return res;
}

} // namespace bgeom
